import { Component, OnInit } from '@angular/core';
import {SearchServiceService} from '../../service/search-service.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public cities;
  public searchInput;
  public urlParam;
  public showEmptyErrorMessage = false;
  public showErrorMessage = false;

  constructor(private searchService: SearchServiceService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.searchInput = '';
    this.activatedRoute.queryParams.subscribe(params => {
      this.urlParam = params.q;
      if (this.urlParam) {
        this.searchInput = this.urlParam;
        this.search();
      }
    });


  }

  valueChange(value) {
    this.searchInput = value;
  }

  public search() {
    this.showEmptyErrorMessage = false;
    const input = this.searchInput;
    if (input.length < 3) {
      this.showErrorMessage = true;
      return;
    }
    this.searchService.getCities(input).subscribe(data => {
      this.showErrorMessage = false;
      this.cities = data.list;
      if (!this.cities) {
        this.showEmptyErrorMessage = true;
      }
      }
    );
  }

}

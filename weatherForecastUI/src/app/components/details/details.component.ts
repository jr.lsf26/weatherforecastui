import { Component, OnInit } from '@angular/core';
import {SearchServiceService} from '../../service/search-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import { months } from 'src/app/utils/formatters';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private searchService: SearchServiceService, private activatedRoute: ActivatedRoute, private router: Router) { }


  public cityId;
  public forecastData;
  public searchInput;
  public weatherData;
  public loaded;


  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.cityId = params.cityId;
    });

    this.searchService.getWeather(this.cityId).subscribe(data => {
      this.weatherData = data;
    });

    this.searchService.getForecast(this.cityId).subscribe(data => {
      this.forecastData = data;
      this.loaded = true;
      this.searchInput = data?.city?.name;
    });
  }
  fixTemp(temp) {
    temp = Math.round(temp * 10) / 10;
    return temp;
  }

  fixDateAndTime(date) {
    const dateFormat = new Date(date);
    const hourString = dateFormat.getHours() < 10 ? '0' + dateFormat.getHours().toString() : dateFormat.getHours().toString();
    const minuteString = dateFormat.getMinutes() < 10 ? '0' + dateFormat.getMinutes().toString() : dateFormat.getMinutes().toString();

    const dateString = hourString + ':' + minuteString + ' ' + months[dateFormat.getMonth()] + ' ' + dateFormat.getDate().toString();
    return dateString;
  }

  valueChange(value) {
    this.searchInput = value;
  }

  search() {
    this.router.navigate([''], {queryParams: {q: this.searchInput}});
  }

  goHome() {
    this.router.navigate(['']);
  }

}

# WeatherForecastUI

A simple UI implementation to the openweather API data.

## Specifications:
    - Angular v9.0
    - Typescript v3.7.5

## Getting started 

Run `npm i` to install dependencies after cloning at the project's root directory.
After all dependencies have been installed, run `ng serve` to run the project at localhost:4200.
